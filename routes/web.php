<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['prefix' => '/api/v1/'], function(){
	
	Route::resource('field', 'FieldController',[
	'except' => ['create', 'edit']]);

	Route::resource('book', 'BookingController',[
	'except' => ['create', 'edit', 'update']]);

	Route::get('book/{id}/history', 'BookingController@getBookingHistory');
	Route::get('book/{id}/booked', 'BookingController@getFieldBookingTimings');
	Route::post('field/search', 'FieldController@getSearchFields');
	Route::post('field/available', 'FieldController@getAvailableFields');
	Route::get('user/book', 'UserController@userBookings');
	Route::post('user/signup', 'UserController@signup');
	Route::post('user/signin', 'UserController@signin');
	Route::get('user/view', 'UserController@getUserInfo');	
	Route::get('user/signout', 'UserController@signout');
});
