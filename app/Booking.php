<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
	use SoftDeletes;

	protected $fillable = ['field_id', 'start_time', 'end_time', 'total_price'];
	protected $dates = ['deleted_at'];

	public function field(){
		return $this->belongsTo('App\Field');
	}

    public function user(){
    	return $this->belongsTo('App\User');
    }
}
