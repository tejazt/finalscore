<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

class UserController extends Controller
{
    public function signup(Request $request){

    	$this->validate($request, [
            'name' => 'required|min:4|max:20',
            'email' => 'required|email',
            'password' => 'required'
		]);
         
        $name = $request->input('name');
        $email = $request->input('email');
        $password = bcrypt($request->input('password'));

        $user = new User([
            'name' => $name,
            'email' => $email,
            'password' => $password
        ]);

        if(!$user->save()){
            $response = [
                'message' => 'Error occured while creating User',
            ];

            return response()->json($response, 404);
        }

        $user->signin = [
            'href' => 'api/v1/user/signin',
            'method' => 'POST',
            'params' => 'email,password'
        ];

        $response = [
            'message' => 'User Created',
            'user' => $user
        ];

        return response()->json($response, 201);
    }

    public function signin(Request $request){
    	$this->validate($request,[
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $credentials = $request->only('email','password');

        try{
            if(! $token = JWTAuth::attempt($credentials)){
                return response()->json(['message' => 'Invalid Credentials!'], 401);
            }
        }catch(JWTException $e){
            return response()->json(['message' => 'Could not generate token'], 500);
        }
        $user = User::where('email', $request->email)->first();
        return response()->json(['token' => $token, 'user' => $user]);
    }

    public function getUserInfo(){
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message' => 'Cannot find user'], 404);
        }
        return response()->json($user, 200);
    }

    public function userBookings(){
        
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message' => 'Cannot find user'], 404);
        }

        $bookings = $user->bookings;
        
        if(!$bookings)
            return response()->json(['message' => 'You have no bookings currently'], 200);

        foreach ($bookings as $booking) {
            $booking->view_booking = [
                'href' => 'api/v1/book/'.$booking->id,
                'method' => 'GET'
            ];
            $booking->view_field = [
                'field_id' => $booking->field->id,
                'field_name' => $booking->field->name,
                'field_no' => $booking->field->field
            ];
        }

        $response = [
            'message' => 'List of all bookings by user',
            'bookings' => $bookings
        ];

        return response()->json($response, 200);
    }

    public function signout(){
        if(JWTAuth::invalidate(JWTAuth::getToken())){
            return response()->json(['message' => 'User logged out'], 200);
        }
        return response()->json(['message' => 'Couldn\'t not log out user'],404);
    }
}
