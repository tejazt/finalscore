<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Field;
use App\Booking;

class FieldController extends Controller
{

    public function __construct(){
        $this->middleware('jwt.auth', ['only' => [
            'store', 'update', 'destroy',
        ]]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fields = Field::all();

        foreach ($fields as $field) {
            $field->view_field = [
                'href' => 'api/v1/field/'.$field->id,
                'method' => 'GET'
            ];
        }

        $response = [
            'message' => 'List of all Fields',
            'fields' => $fields,
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'field' => 'required',
            'price' => 'required',
            'address' => 'required',
            ]);
         
        $name = $request->input('name');
        $fieldName = $request->input('field');
        $price = $request->input('price');
        $address = $request->input('address');

        $field = new Field([
            'name' => $name,
            'field' => $fieldName,
            'price' => $price,
            'address' => $address,
        ]);

        if(!$field->save()){
            $response = [
                'message' => 'Error in creating Field',
            ];

            return response()->json($response, 404);
        }

        $field->view_field = [
            'href' => 'api/v1/field/'.$field->id,
            'method' => 'GET'
        ];

        $response = [
            'message' => 'Field Created',
            'field' => $field,
        ];

        return response()->json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $field = Field::findOrFail($id);
        $field->view_field = [
            'href' => 'api/v1/field/'.$field->id,
            'method' => 'GET'
        ];

        $response = [
            'message' => 'Field Information',
            'field' => $field
        ];
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'field' => 'required',
            'price' => 'required',
            'address' => 'required',
            ]);
         
        $name = $request->input('name');
        $fieldName = $request->input('field');
        $price = $request->input('price');
        $address = $request->input('address');

        $field = Field::find($id);
        $field->name = $name;
        $field->field = $fieldName;
        $field->price = $price;
        $field->address = $address;

        if(!$field->update()){
            
            $response = [
                'message' => 'Error in creating Field',
            ];

            return response()->json($response, 404);
        }

        $field->view_field = [
            'href' => 'api/v1/field/'.$field->id,
            'method' => 'GET'
        ];

        $response = [
            'message' => 'Field Updated',
            'field' => $field,
        ];

        return response()->json($response, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = [
            'message' => 'Field Deleted',
            'create' => [
                'href' => 'api/v1/field',
                'method' => 'POST',
                'params' => 'name, field, price, address'
            ]
        ];
        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAvailableFields(Request $request)
    {
        $this->validate($request, [
            'start_time' => 'required',
            'end_time' => 'required',
        ]);

        $start_time = $request['start_time'];
        $end_time = $request['end_time'];
        
        $invalidCase1 = Booking::where('start_time', '>=', $start_time)
            ->where('start_time', '<', $end_time)
            ->pluck('field_id');
        $invalidCase2 = Booking::where('end_time', '>', $start_time)
            ->where('end_time', '<=', $end_time)
            ->pluck('field_id');

        $inavailFields = array_merge(json_decode($invalidCase1), json_decode($invalidCase2));

        $fields = Field::whereNotIn('id', $inavailFields)->get();

        $i = 0;
        foreach ($fields as $field) {
            $field->view_field = [
                'href' => 'api/v1/field/'.$field->id,
                'method' => 'GET'
            ];
        }

        $response = [
            'message' => 'List of all Fields',
            'fields' => $fields,
        ];

        return response()->json($response, 200);
    }

    public function getSearchFields(Request $request){
        $this->validate($request, [
            'name' => 'required'
        ]);
        $name = $request['name'];
        $fields = Field::where('name', 'like', '%'.$name.'%')->get();
        $response = [
            'message' => 'List of all Fields',
            'fields' => $fields,
        ];
        return response()->json($response, 200);
    }
}
