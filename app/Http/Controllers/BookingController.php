<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;
use App\Field;
use App\User;
use JWTAuth;

class BookingController extends Controller
{

    public function __construct(){
        $this->middleware('jwt.auth', ['except' => ['getFieldBookingTimings', 'getBookingHistory']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $bookings = Booking::all();

        foreach ($bookings as $booking) {
            $booking->view_booking = [
                'href' => 'api/v1/book/'.$booking->id,
                'method' => 'GET'
            ];
        }

        $response = [
            'message' => 'List of all bookings',
            'bookings' => $bookings
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'field_id' => 'required|numeric',
            'start_time' => 'required|numeric',
            'end_time' => 'required|numeric',
        ]);

        $field_id = $request->input('field_id');
        $start_time = $request->input('start_time');
        $end_time = $request->input('end_time');

        $field = Field::findorFail($field_id);

        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message' => 'Cannot find user'], 404);
        }

        $booking = new Booking([
            'field_id' => $field_id,
            'start_time' => $start_time,
            'end_time' => $end_time,
            'total_price' => ($field->price * ($end_time - $start_time)),
        ]);

        if(!$user->bookings()->save($booking)){
            $response = [
            'message' => 'Error while booking field',
            ];

            return response()->json($response, 404);
        }

        $booking->view_booking = [
            'href' => 'api/v1/book/'.$booking->id,
            'method' => 'GET'
        ];

        $booking->cancel = [
            'href' => 'api/v1/book/'.$booking->id,
            'method' => 'DELETE'
        ];

        $response = [
            'message' => $field->field.' in '.$field->name.' has been booked',
            'booking' => $booking
        ];

        return response()->json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking = Booking::with('user')->findOrFail($id);
        $booking->view_booking = [
            'href' => 'api/v1/book/'.$booking->id,
            'method' => 'GET'
        ];

        $response = [
            'message' => 'Booking Information',
            'booking' => $booking
        ];
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json(['message' => 'Cannot find user'], 404);
        }

        $booking = Booking::findOrFail($id);
        if($booking->user->id != $user->id){
            return response()->json(['message' => 'User has not booked field and hence cannot cancel booking', 401]);
        }

        if(!$booking->delete()){

            $response = ['message' => 'Error deleting meeting'];

            return response()->json($response, 404);
        }

        $response = [
            'message' => 'Booking Deleted',
            'create' => [
                'href' => 'api/v1/book',
                'method' => 'POST',
                'params' => 'field_id, start_time, end_time, total_price'
            ]
        ];
        return response()->json($response, 200);
    }

    public function getFieldBookingTimings($id){
        $timings = Booking::where('field_id', $id)
                    ->select('start_time', 'end_time')
                    ->orderBy('start_time', 'asc')
                    ->get();
        $response = [
            'message' => 'Currently booked timings of field',
            'timings' => $timings,
        ];
        return response()->json($response, 200);
    }

    public function getBookingHistory($id){

        $bookings = Booking::withTrashed()
                    ->where('user_id', $id)
                    ->orderBy('created_at', 'asc')
                    ->get();

        foreach ($bookings as $booking) {
            $booking->view_booking = [
                'href' => 'api/v1/book/'.$booking->id,
                'method' => 'GET'
            ];
            $booking->view_field = [
                'field_id' => $booking->field->id,
                'field_name' => $booking->field->name,
                'field_no' => $booking->field->field
            ];
        }


        $response = [
            'message' => 'Booking history of user',
            'bookings' => $bookings,
        ];
        return response()->json($response, 200);
    }
}
